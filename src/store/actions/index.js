import Axios from 'axios';
import store from '..';

export const Type = {
    LOAD_DATA: 'LOAD_DATA',
    LOAD_DATA_SUCCESS: 'LOAD_DATA_SUCCESS',
    LOAD_DATA_ERROR: 'LOAD_DATA_ERROR',
    FILTER_DATA: 'FILTER_DATA',
}

export const loadData = () => {
    return {
        type: Type.LOAD_DATA,
    }
}

export const loadDataSuccess = (data) => {
    return {
        type: Type.LOAD_DATA_SUCCESS,
        data,
    }
}

export const filterData = (filters) => {
    return {
        type: Type.FILTER_DATA,
        filters,
    }
}

export const loadDataError = (error) => {
    return {
        type: Type.LOAD_DATA_ERROR,
        error,
    }
}

export const execFetch = () => {
    const url = 'https://football-players-b31f2.firebaseio.com/players.json?print=pretty';
    store.dispatch(loadData());
    return (dispatch) => Axios.get(url)
        .then((response) => {
            if (!response.data) {
                throw new Error('Cannot get the data!');
            }
            const players = response.data.map((player) => {
                return { ...player, age: getAge(player.dateOfBirth) }
            });

            dispatch(loadDataSuccess(players));
        })
        .catch((err) => dispatch(loadDataError(err)));
}

function getAge(dateOfBirth) {
    const today = new Date();
    const birthDate = new Date(dateOfBirth);
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age -= 1;
    }

    return age;
}
