import { createSelector } from 'reselect';

export const getPlayers = (state) => state.players;
export const getPlayersFull = (state) => state.playersFull;

export const getPositions = createSelector(
    getPlayersFull,
    (players) => {
        const filters = players.map((player) => player.position);
        return Array.from(new Set(filters)).sort();
    }
)