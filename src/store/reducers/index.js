export const initialState = {
    loading: false,
    playersFull: [],
    players: [],
    error: null,
};

function rootReducer(state = initialState, action) {
    switch (action.type) {
        case 'LOAD_DATA':
            return { ...state, loading: true };
        case 'LOAD_DATA_SUCCESS':
            return { ...state, loading: false, players: action.data, playersFull: action.data };
        case 'FILTER_DATA':
            let players = state.playersFull;
            if (Object.keys(action.filters).some((filter) => action.filters[filter])) {
                Object.keys(action.filters).forEach((filter) => {
                    if (!action.filters[filter] || !action.filters[filter] > 0) {
                        delete action.filters[filter]
                    }
                })

                players = players.filter((player) => {
                    const filter = { ...player, ...action.filters }
                    return JSON.stringify(player) === JSON.stringify(filter)
                })

            }
            return { ...state, players };
        case 'LOAD_DATA_ERROR':
            return { ...state, loading: false, error: action.error };
        default:
            return state
    }
};

export default rootReducer;