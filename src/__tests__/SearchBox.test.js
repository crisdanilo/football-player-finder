import React from 'react';
import { mount } from 'enzyme';
import { SearchBox } from '../containers/search-box/SearchBox';
import sinon from 'sinon';

let searchBox, form;
const props = {
  positions: [
    'Attacking Midfield',
    'Central Midfield',
    'Centre-Back',
    'Centre-Forward',
    'Defensive Midfield',
    'Keeper',
    'Left Midfield',
    'Left Wing',
    'Left-Back',
    'Right-Back'
  ],
  dispatch: jest.fn(),
}

beforeEach(() => {
  sinon.spy(SearchBox.prototype, 'handleInputOnBlurAge');
  sinon.spy(SearchBox.prototype, 'handleInputOnFocusAge');
  sinon.spy(SearchBox.prototype, 'handleInputChange');
  sinon.spy(SearchBox.prototype, 'handleSubmit');
  searchBox = mount(<SearchBox {...props} />)
  form = searchBox.find('form');
})

afterEach(() => {
  SearchBox.prototype.handleInputOnBlurAge.restore();
  SearchBox.prototype.handleInputOnFocusAge.restore();
  SearchBox.prototype.handleInputChange.restore();
  SearchBox.prototype.handleSubmit.restore();
});

describe('SearchBox Component', () => {
  it('should have an one only form', () => {
    expect(form).toHaveLength(1);
  });

  it('should have a prop positions and should to be an array', () => {
    expect(Array.isArray(searchBox.props().positions)).toBeTruthy();
    expect(searchBox.props().positions).toHaveLength(10);
    expect(searchBox.props().positions).toEqual(props.positions);
  });

  it('should have an element of errors', () => {
    expect(searchBox.find('p').hasClass('errors')).toBeTruthy();
  });

  it('should have a form with 3 fields: 2 inputs and 1 select', () => {
    expect(form.find('input.input')).toHaveLength(2);
    expect(form.find('select')).toHaveLength(1);
  });

  it('should have an one only button submit', () => {
    expect(form.find('input[type="submit"].button')).toHaveLength(1);
    expect(form.find('input[type="submit"].button').instance().value).toBe('Search');
  });

  it('should can add age between 18 and 40 years old', () => {
    const inputAge = form.find('input[name="age"]');
    inputAge.instance().value = 10;
    inputAge.simulate('blur');

    expect(searchBox.state().isError).toEqual(true);
    expect(SearchBox.prototype.handleInputOnBlurAge.calledOnce).toBe(true);

    inputAge.simulate('focus');
    inputAge.instance().value = 20;
    inputAge.simulate('blur');
    expect(searchBox.state().isError).toEqual(false);
    expect(SearchBox.prototype.handleInputOnFocusAge.calledOnce).toBe(true);
  });

  it('should show an error message when isError change to true', () => {
    searchBox.setState({ isError: true });
    expect(searchBox.exists('p.is-danger')).toBeTruthy();
    expect(searchBox.find('p.is-danger').text()).toBe('*The age must to be between 18 and 40 years old');

    searchBox.setState({ isError: false });
    expect(searchBox.exists('p.is-danger')).toBeFalsy();
  });

  it('should update name in status when value change', () => {
    const name = form.find('input[name="name"]');
    name.simulate('change', { target: { value: 'Romelu Lukaku', name: 'name' } })
    expect(SearchBox.prototype.handleInputChange.calledOnce).toBe(true);
    expect(searchBox.state().name).toEqual('Romelu Lukaku');
  });

  it('should update postion in status when value change', () => {
    const position = form.find('select[name="position"]');
    position.simulate('change', { target: { value: 'Central Midfield', name: 'position' } })
    expect(SearchBox.prototype.handleInputChange.calledOnce).toBe(true);
    expect(searchBox.state().position).toEqual('Central Midfield');
  });

  it('should call handleSubmit on submit click', () => {
    searchBox.setState({ name: 'Romelu Lukaku', position: 'Centre-Forward', age: 25 });
    form.simulate('submit');

    expect(searchBox.state().name).toEqual('Romelu Lukaku');
    expect(searchBox.state().position).toEqual('Centre-Forward');
    expect(searchBox.state().age).toEqual(25);

    expect(SearchBox.prototype.handleSubmit.calledOnce).toBe(true);
  });

})