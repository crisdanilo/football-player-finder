import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import { loadData, loadDataSuccess, filterData, execFetch } from '../store/actions';

const mockStore = configureMockStore([thunk]);

describe('Actions', () => {
  let store;

  beforeEach(() => {
    store = mockStore({});
  });

  afterEach(() => {
    nock.cleanAll();
  });

  it('creates LOAD_DATA_SUCCESS when fetching endpoint has been done', () => {
    nock('https://football-players-b31f2.firebaseio.com/', { allowUnmocked: true })
      .get('players.json?print=pretty')
      .reply(200, { answer: 'any' });

    return store.dispatch(execFetch())
      .then(() => {
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('should dispatch LOAD_DATA action', () => {
    expect(loadData()).toEqual({ type: 'LOAD_DATA' });
  });

  it('should dispatch FILTER_DATA action', () => {
    const filters = {
      name: 'Romelu Lukaku',
      position: 'Centre-Forward',
      age: 25
    }
    expect(filterData(filters)).toEqual({ type: 'FILTER_DATA', filters })
  });
});