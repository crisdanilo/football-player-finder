import reducer, { initialState } from '../store/reducers';

describe('Reducers', () => {

  let state;

  beforeEach(() => {
    state = {
      loading: false,
      playersFull: [{
        "contractUntil": "2022-06-30",
        "dateOfBirth": "1993-05-13",
        "jerseyNumber": 9,
        "name": "Romelu Lukaku",
        "nationality": "Belgium",
        "position": "Centre-Forward",
        "age": 25
      }, {
        "contractUntil": "2019-06-30",
        "dateOfBirth": "1990-11-07",
        "jerseyNumber": 1,
        "name": "David de Gea",
        "nationality": "Spain",
        "position": "Keeper",
        "age": 28
      }, {
        "contractUntil": "2021-06-30",
        "dateOfBirth": "1987-02-22",
        "jerseyNumber": 20,
        "name": "Sergio Romero",
        "nationality": "Argentina",
        "position": "Keeper",
        "age": 32
      }]
      ,
      players: [],
      error: null,
    };
  });

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toMatchSnapshot();
  });

  it('should handle LOAD_DATA', () => {
    expect(reducer(initialState, { type: 'LOAD_DATA' })).toMatchSnapshot();
  });

  it('should handle LOAD_DATA_SUCCESS', () => {
    expect(reducer(initialState, { type: 'LOAD_DATA_SUCCESS', data: state })).toMatchSnapshot();
  });

  it('should handle LOAD_DATA_ERROR', () => {
    expect(reducer(initialState, { type: 'LOAD_DATA_ERROR', error: { message: 'Not Found!' } })).toMatchSnapshot();
  });

  it('should handle FILTER_DATA', () => {
    const filters = {
      name: 'Romelu Lukaku',
      position: 'Centre-Forward',
      age: 25
    }
    expect(reducer(state, { type: 'FILTER_DATA', filters })).toMatchSnapshot();
  });
})
