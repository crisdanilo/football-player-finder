import React from 'react';
import { shallow } from 'enzyme';
import AppContainer, { App } from '../containers/App';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import SearchBox from '../containers/search-box/SearchBox';
import Players from '../components/players/Players';
import sinon from 'sinon';

const props = {
  players: [
    {
      "contractUntil": "2022-06-30",
      "dateOfBirth": "1993-05-13",
      "jerseyNumber": 9,
      "name": "Romelu Lukaku",
      "nationality": "Belgium",
      "position": "Centre-Forward",
      "age": 25
    },
    {
      "contractUntil": "2019-06-30",
      "dateOfBirth": "1990-11-07",
      "jerseyNumber": 1,
      "name": "David de Gea",
      "nationality": "Spain",
      "position": "Keeper",
      "age": 28
    }
  ],
  positions: [],
  getData: jest.fn(),
}

describe('App Component', () => {
  let appComponent, getData;

  beforeEach(() => {
    appComponent = shallow(<App {...props} />);
    getData = sinon.stub(props, 'getData');
  })

  afterEach(() => {
    getData.restore();
  })

  it('should have a title', () => {
    expect(appComponent.find('.title').text()).toBe('Football Player Finder');
  })

  it('should have a SearchBox component', () => {
    expect(appComponent.contains(<SearchBox positions={props.positions} />)).toBe(true);
  });

  it('should have a Players component', () => {
    expect(appComponent.contains(<Players players={props.players} />)).toBe(true);
  });

  it('should execut getData in ComponentDidMount', () => {
    appComponent = shallow(<App {...props} />);
    expect(getData.calledOnce).toBe(true)
  })
})

describe('MapState in App Component', () => {
  // create any initial state needed
  const initialState = {
    loading: false,
    playersFull: props.players,
    players: props.players,
    error: null,
  };

  let provider, store;
  const mockStore = configureMockStore([thunk]);

  beforeAll(() => {
    store = mockStore(initialState);
    provider = shallow(<AppContainer store={store} {...props} />).find(App);
  })

  it('should have a prop players', () => {
    expect(provider.props().players).toHaveLength(2);
  });

  it('should have a prop positions', () => {
    expect(provider.props().positions).toHaveLength(2);
  });
})