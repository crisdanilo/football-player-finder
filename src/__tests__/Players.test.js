import React from 'react';
import { mount } from 'enzyme';
import { Players } from '../components/players/Players';

let playersComponent;
const props = {
  players: [
    {
      "contractUntil": "2022-06-30",
      "dateOfBirth": "1993-05-13",
      "jerseyNumber": 9,
      "name": "Romelu Lukaku",
      "nationality": "Belgium",
      "position": "Centre-Forward",
      "age": 25
    },
    {
      "contractUntil": "2019-06-30",
      "dateOfBirth": "1990-11-07",
      "jerseyNumber": 1,
      "name": "David de Gea",
      "nationality": "Spain",
      "position": "Keeper",
      "age": 28
    }
  ]
}

beforeEach(() => {
  playersComponent = mount(<Players {...props} />)
})

describe('Players Component', () => {
  it('should have an one only table', () => {
    expect(playersComponent.find('table.table')).toHaveLength(1);
  });

  it('should have like heads: Player, Position, Team and Age', () => {
    const thead = playersComponent.find('table > thead')
    expect(thead.find('tr > th')).toHaveLength(4);
    thead.find('tr > th').forEach((th) => {
      expect(th.text()).toMatch(/(Player|Position|Team|Age)/)
    })
  });

  it('should have an array like players', () => {
    expect(Array.isArray(playersComponent.props().players)).toBeTruthy();
  });

  it('should have an array of 2 element in players prop', () => {
    expect(playersComponent.props().players).toHaveLength(2);
    expect(playersComponent.props().players).toEqual(props.players);
  });

  it('The player object should have a property age and must to be between 18 and 40 years old', () => {
    playersComponent.props().players.forEach((player) => {
      expect(player).toHaveProperty('age');
      expect(player.age).toBeGreaterThanOrEqual(18);
      expect(player.age).toBeLessThanOrEqual(40);
    })
  });
})