import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';

import './SearchBox.sass';
import { filterData } from '../../store/actions';

export class SearchBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            position: null,
            age: 0,
            isError: false,
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputOnBlurAge = this.handleInputOnBlurAge.bind(this);
        this.handleInputOnFocusAge = this.handleInputOnFocusAge.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const { value, name } = event.target;

        this.setState({
            [name]: null
        });

        if (value.length) {
            this.setState({
                [name]: value
            });
        }

    }

    handleInputOnFocusAge() {
        this.setState({ isError: false })
    }

    handleInputOnBlurAge(event) {
        const { value } = event.target;
        if ((value < 18 || value > 40) && value.length) {
            this.setState({ isError: true })
        } else {
            this.setState({ age: Number(value) })
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        if (!this.state.isError) {
            const filters = (({ name, position, age }) => ({ name, position, age }))(this.state);
            this.props.dispatch(filterData(filters));
        }
    }

    render() {
        const positions = this.props.positions;
        const options = positions.map((position) =>
            <option key={position}>{position}</option>
        );

        let error = classnames('errors is-invisible')
        let styleAge = ['input']

        if (this.state.isError) {
            error = classnames(['help', 'is-danger']);
            styleAge.push('is-danger');
        }

        const classAge = classnames(styleAge)

        return (
            <div>
                <form className="field is-horizontal" onSubmit={this.handleSubmit}>
                    <div className="field-body">
                        <div className="field">
                            <div className="control">
                                <input name="name" className="input" type="text" placeholder="Player Name" onChange={this.handleInputChange} />
                            </div>
                        </div>
                        <div className="field is-narrow">
                            <div className="control">
                                <div className="select">
                                    <select name="position" onChange={this.handleInputChange} >
                                        <option value="">Select position...</option>
                                        {options}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="field input-age is-narrow">
                            <div className="control">
                                <input
                                    name="age"
                                    pattern="[0-9]*"
                                    min="18" max="40"
                                    className={classAge}
                                    type="number"
                                    placeholder="Age"
                                    onBlur={this.handleInputOnBlurAge}
                                    onFocus={this.handleInputOnFocusAge}
                                />
                            </div>
                        </div>
                        <div className="field">
                            <div className="control">
                                <input type="submit" className="button is-primary" value="Search" />
                            </div>
                        </div>
                    </div>
                </form>
                <p className={error}>*The age must to be between 18 and 40 years old</p>
            </div>
        );
    }
}

export default connect()(SearchBox);