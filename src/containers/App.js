import React, { Component, Suspense } from 'react';
import { connect } from 'react-redux';

import './App.sass';
import Players from '../components/players/Players';
import SearchBox from './search-box/SearchBox';

import { execFetch } from '../store/actions';
import { getPositions, getPlayers } from '../store/selectors';

const mapStateToProps = state => {
  return {
    players: getPlayers(state),
    positions: getPositions(state)
  };
};

export class App extends Component {
  componentDidMount() {
    this.props.getData();
  }

  render() {
    return (
      <div className="wrapper">
        <h4 className="title is-4">Football Player Finder</h4>
        <SearchBox positions={this.props.positions} />
        <Suspense fallback={<div>Loading...</div>}>
          <Players players={this.props.players} />
        </Suspense>
      </div>
    );
  }
}

export default connect(mapStateToProps, { getData: execFetch })(App);
