import React from 'react';
import PropTypes from 'prop-types';

export const Players = (props) => {
    const players = props.players.map((player) =>
        <tr key={player.jerseyNumber}>
            <td>{player.name}</td>
            <td>{player.position}</td>
            <td>{player.nationality}</td>
            <td>{player.age}</td>
        </tr>
    );

    return (
        <table className="table is-striped is-hoverable is-fullwidth">
            <thead>
                <tr>
                    <th>Player</th>
                    <th>Position</th>
                    <th>Team</th>
                    <th>Age</th>
                </tr>
            </thead>
            <tbody>
                {players}
            </tbody>
        </table>
    );
}

Players.propTypes = {
    players: PropTypes.arrayOf(PropTypes.shape({
        jerseyNumber: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        position: PropTypes.string.isRequired,
        nationality: PropTypes.string,
        age: PropTypes.number.isRequired,
    })).isRequired,
};

export default Players;